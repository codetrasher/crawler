// crawler project crawler.go
package crawler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/codetrasher/crawler/parser"
	"bitbucket.org/codetrasher/gostack"
)

var (
	root      string
	depth     int
	pattern   []string
	userAgent string

	crawlRules  rules
	processUrls gostack.Stack

//	log     bool
)

// Builder method for crawler.
func Build(_root string, _depth int, _pattern string, _userAgent string) {
	root = _root
	depth = _depth
	userAgent = _userAgent

	// Build link parsing pattern
	pattern = strings.Split(_pattern, "|")
	parser.SetPattern(pattern)

	crawlRules := rules{make([]string, 0), make([]string, 0), 0}
	crawlRules.SetRules(findRobots())

	//	processUrls.Push(root) // Root is the first page to crawl & scrape so we push that one first

}

/*
 * Main function for the crawler. Loop according to given depth.
 */
func Run() {

	// Make a copy of urlStack and use that one for storing all URLs.
	//	var storedUrls gostack.Stack
	//	storedUrls = processUrls

	//	fmt.Printf("urlStack: %v\n", processUrls)

	// Print URLs by depth
	//	var p func(int, string)
	//	p = func(d int, s string) {

	//		if s == root {
	//			return
	//		}

	//		var indent func(*int) string
	//		indent = func(d *int) string {
	//			t := ""
	//			for i := 0; i < *d+1; i++ {
	//				t += "===="
	//			}
	//			t += ">"

	//			return t
	//		}

	//		fmt.Println(indent(&d) + s)
	//	}

	// Print the root page
	//	fmt.Println(root)

	//	for i := 0; i < depth+1; i++ {

	//		for j := 0; j < processUrls.Len(); j++ {

	//			//			fmt.Printf("\nprocessUrls: %v\n", processUrls)

	//			if currUrl, ok := processUrls.Pop().(string); ok {

	//				p(i, currUrl)

	//				b, h := executeRequest("GET", "http://"+currUrl)

	//				//				fmt.Printf("Body: \n%s\n\n", string(b))
	//				//				fmt.Printf("Header: \n%v\n\n", h)

	//				l := parser.Parse(h.Get("Content-Type"), b)

	//				//				go func() {
	//				for _, v := range l {
	//					processUrls.Push(v)
	//					storedUrls.Push(v)
	//				}
	//				//				}()

	//			}
	//		}

	//	}

	// Call with zero as starting depth
	runRecursive(0, root)
}

// Private method. Run crawling/scraping method recursively.
func runRecursive(n int, url string) /*(newUrls gostack.Stack)*/ {
	httpExec := false
	var p func(int, string)
	p = func(d int, s string) {

		if s == root {
			fmt.Println(root)
			return
		}

		var indent func(*int) string
		indent = func(d *int) string {
			t := ""
			for i := 0; i < *d+1; i++ {
				t += "===="
			}
			t += ">"

			return t
		}

		fmt.Println(indent(&d) + s)
	}

	urls := gostack.Stack{}
	urls.Push(url)

	j := urls.Len()
	for j > 0 {

		//		fmt.Printf("urls: %v\n\n", urls)
		//		fmt.Printf("depth: %d\n\n", n)
		//			fmt.Printf("\nprocessUrls: %v\n", processUrls)
		if urls.Len() <= 0 {
			break
		}

		if currUrl, ok := urls.Pop().(string); ok {

			p(n, currUrl)

			var b []byte
			var h http.Header

			if !httpExec {
				if n == 0 {
					//					p(n, currUrl)
					b, h = executeRequest("GET", "http://"+root)
					httpExec = true
				} else if n > 0 {
					//					p(n, currUrl)
					b, h = executeRequest("GET", "http://"+root+currUrl)
					httpExec = true
				}

				l := parser.Parse(h.Get("Content-Type"), b)

				// Do twice in order to get rid off backwards stack
				for _, v := range l {
					urls.Push(v)
					urls.Reverse()
					//					s := gostack.Stack{}
				}

				// Set loop index back to 0
				j = urls.Len()

				//				fmt.Printf("urls.Len(): %d\n\n", urls.Len())
				//				fmt.Printf("new index: %d\n\n", j)
			}

			//				fmt.Printf("Body: \n%s\n\n", string(b))
			//				fmt.Printf("Header: \n%v\n\n", h)

			//			l := parser.Parse(h.Get("Content-Type"), b)

			//				go func() {
			//			for _, v := range l {
			//				urls.Push(v)
			//				//					storedUrls.Push(v)
			//			}
			//				}()

		}

		if depth > 0 && n < depth {

			if urls.Len() > 0 {
				runRecursive(n+1, urls.Pop().(string))
			}
		}

		j -= 1
	}

	//	}

	return
}

func findRobots() string {

	robots := "robots.txt"

	body, _ := executeRequest("GET", "http://"+root+"/"+robots)

	return string(body)
}

func executeRequest(method string, target string) (body []byte, header http.Header) {
	//	fmt.Printf("executeRequest: method = %s, target = %s\n", method, target)

	req, _ := http.NewRequest(method, target, nil)
	//	req.Header.Set("Content-Type", "text/html")
	req.Header.Set("User-Agent", userAgent)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//		println(err.Error())
		return
	}

	defer resp.Body.Close()
	body, _ = ioutil.ReadAll(resp.Body)
	header = resp.Header

	//	fmt.Printf("Response: \n%s\n\n", string(body))
	//	fmt.Printf("Headers: \n%v\n\n", header)

	return
}
