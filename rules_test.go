package crawler

import (
	"fmt"
	"testing"
)

var (
	robotsContent string = `User-agent: *
Disallow: /redir.php
Disallow: /xml.php
Allowed: /testikansio
Allowed: *.js

User-agent: Googlebot
Disallow: ?h=
Disallow: ?p=
Disallow: &p=
Disallow: ?pg=
Disallow: ?s=
Disallow: &s=
Disallow: ?v=
Disallow: /index.php
Disallow: /redir.php
Disallow: /xml.php
Disallow: *?h=
Disallow: *?p=
Disallow: *&p=
Disallow: *?pg=
Disallow: *?s=
Disallow: *&s=
Disallow: *?v=`
)

func TestSet(t *testing.T) {

	r := new(rules)

	if r == nil {
		t.Fail()
	}

	r.Set(robotsContent)
	fmt.Printf("Rules: %v", r)

	if r.allowed == nil || r.disallowed == nil {
		t.Fail()
	}
}
