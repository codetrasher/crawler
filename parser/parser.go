// parser project parser.go
package parser

import (
	//	"fmt"
	"sort"
	"strings"

	"golang.org/x/net/html"
)

var (
	contentTypeStr = [...]string{"text/html", "application/json"}

	pattern map[string]string = make(map[string]string)
)

func SetPattern(_pattern []string) {
	pattern["href"] = _pattern[0]

	for _, v := range _pattern {
		var s []string
		if strings.Contains(v, "subpath") {
			s = strings.Split(v, "=")
			pattern["subpath"] = s[1]
		} else if strings.Contains(v, "keyword") {
			s = strings.Split(v, "=")
			pattern["keyword"] = s[1]
		}
	}
}

func Parse(contentType string, body []byte) (l []string) {

	s := strings.Split(contentType, ";")
	contentType = s[0]

	//	fmt.Printf("Parse:: contentType: %s\n\n", contentType)
	//	fmt.Printf("\n\nParser:: body: %s\n\n", string(body))
	//	fmt.Printf("Parse:: contentTypeStr: %v\n", contentTypeStr)

	switch contentType {
	case contentTypeStr[0]:
		parseHtml(&body, &l)
	case contentTypeStr[1]:
		parseJson(&body, &l)
	}

	l = removeDuplicates(l)
	//	fmt.Printf("Parsed list: %v\n", l)

	return
}

func parseHtml(b *[]byte, l *[]string) {

	//	fmt.Println("\nparseHTML\n")

	s := string(*b)
	doc, _ := html.Parse(strings.NewReader(s))

	//	fmt.Printf("\nDoc: %v\n", doc)

	var f func(*html.Node)
	f = func(n *html.Node) {

		//		fmt.Printf("\nType: %v\n", n.Type)
		//		fmt.Printf("\nData: %v\n", n.Data)

		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" && strings.Contains(a.Val, pattern["subpath"]) {
					*l = append(*l, a.Val)
					//					fmt.Printf("\nParsed link: %s\n", l)

					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	//	fmt.Printf("\nLinks: %s", l)

	return
}

func parseJson(b *[]byte, l *[]string) {

}

func removeDuplicates(s []string) []string {

	// First, sort the array
	sort.Strings(s)

	//	fmt.Printf("Sorted URLs: %v\n\n", s)

	// New slice
	n := make([]string, 0)

	// Iterate through the list and remove duplicates
	for i := 0; i < len(s); i++ {

		exists := false
		for e := 0; e < i; e++ {
			if s[e] == s[i] {
				exists = true
				break
			}
		}

		if !exists {
			n = append(n, s[i])
		}
	}

	//	fmt.Printf("Original: %v\n\n", s)
	//	fmt.Printf("Modified: %v\n\n", n)

	return n
}
