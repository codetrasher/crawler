package crawler

import (
	"bufio"
	//	"fmt"
	"strings"
)

type rules struct {
	allowed    []string
	disallowed []string
	crawlDelay int
}

func (r *rules) SetRules(robotsContent string) {

	rd := strings.NewReader(robotsContent)
	sc := bufio.NewScanner(rd)

	flagAllCrawlers := false

	// If robots.txt isn't found, crawling is assumed to be allowed.
	if robotsContent == "" {
		r.allowed = append(r.allowed, "/")
	}

	for sc.Scan() {

		line := sc.Text()

		// Rules for every crawler
		if strings.Contains(line, "User-agent: *") {
			flagAllCrawlers = true
		}

		if line == "" {
			flagAllCrawlers = false
		}

		if flagAllCrawlers == true {

			if strings.Contains(line, "Disallow") {
				str := strings.SplitAfter(line, ": ")
				r.disallowed = append(r.disallowed, str[1])

				//				fmt.Printf("Str: %s\n", str)
				//				fmt.Printf("r.disallowed: %s\n", r.disallowed)
			} else if strings.Contains(line, "Allowed") {
				str := strings.SplitAfter(line, ": ")
				r.allowed = append(r.allowed, str[1])

				//				fmt.Printf("Str: %s\n", str)
				//				fmt.Printf("r.allowed: %s\n", r.allowed)
			}
		}
	}

}
